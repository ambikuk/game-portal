<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;

class GameQuery extends \yii\db\ActiveQuery {
    
    public function bySlug($slug){
        $model = $this->andWhere(['slug'=>$slug])->one();
        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function me() {
        $this->andWhere(['user_id' => Yii::$app->loggedin->user->id]);
        return $this;
    }

    public function active() {
        $this->andWhere(['status' => Game::STATUS_ACTIVE]);
        return $this;
    }

    public function search($search) {
        $this->andWhere('title like :search', [':search' => "%" . $search . "%"]);
        return $this;
    }

    public function filterByCategory($category) {
        if ($category > 0)
            $this->andWhere(['category_id' => $category]);
        return $this;
    }
    
    public function getLatest10(){
        return $this->active()->orderBy('id desc')->limit(10)->all();
    }
    
    public function getTotalGame(){
        return $this->active()->count();
    }

}
