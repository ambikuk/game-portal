<?php

namespace common\models;

class GameCommentQuery extends \yii\db\ActiveQuery {

    public function gameId($id) {
        $this->andWhere(['game_id' => $id]);
        return $this;
    }

    public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    
    
    public function getTotalGameComment(){
        return $this->active()->count();
    }
    

}
