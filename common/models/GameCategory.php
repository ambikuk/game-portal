<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "game_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Game[] $games
 */
class GameCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GameCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameCategoryQuery(get_called_class());
    }
}
