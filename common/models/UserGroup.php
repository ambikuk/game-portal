<?php

namespace common\models;

use Yii;

class UserGroup extends \yii\db\ActiveRecord
{
 
    public static function tableName()
    {
        return 'user_group';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 45]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['group_id' => 'id']);
    }

    public static function find()
    {
        return new UserGroupQuery(get_called_class());
    }
}
