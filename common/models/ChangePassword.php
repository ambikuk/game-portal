<?php

namespace common\models;

use Yii;

class ChangePassword extends User {

    public $old_password;
    public $new_password;
    public $new_password_repeat;

    public function rules()
    {
        return [
            ['old_password', 'validatePass'],
            [['old_password', 'new_password', 'new_password_repeat'], 'required'],
            ['new_password', 'compare', 'compareAttribute' => 'new_password_repeat'],
            ['new_password', 'compare', 'compareAttribute' => 'old_password', 'operator' => '!='],
        ];
    }

    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('app', 'Old Password'),
            'new_password' => Yii::t('app', 'New Password'),
            'new_password_repeat' => Yii::t('app', 'Repeat New Password')
        ];
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setPassword($this->new_password);
            $this->generateAuthKey();
            return true;
        }
        return false;
    }

    public function validatePass()
    {
        if (!$this->hasErrors()) {
            if (!$this->validatePassword($this->old_password)) {
                $this->addError('old_password', 'Incorrect password.');
            }
        }
    }

}
