<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery {

    public function getMyAccount() {
        $this->andWhere('id=:user_id', [':user_id' => Yii::$app->loggedin->user->id]);
        return $this;
    }

    public function administrator() {
        $this->andWhere('group_id=:id', [':id' => User::ADMINISTRATOR]);
        return $this;
    }

    public function findUsernameActive() {
        $this->andWhere('username=:username and status=:status', [':username' => $username, ':status' => User::STATUS_ACTIVE]);
        return $this;
    }

    public function member() {
        $this->where('group_id=:id', [':id' => User::MEMBER]);
        return $this;
    }

    public function active() {
        $this->andWhere(['status' => Game::STATUS_ACTIVE]);
        return $this;
    }
    
    public function getLatest10Members(){
        return $this->member()->active()->orderBy('id desc')->limit(10)->all();
    }
    
    public function getTotalMember(){
        return $this->member()->active()->count();
    }
}
