<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[\app\models\UserGroup]].
 *
 * @see \app\models\UserGroup
 */
class UserGroupQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }

    /**
     * @inheritdoc
     * @return \app\models\UserGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\UserGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}