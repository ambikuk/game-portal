<?php

namespace common\models;

use Yii;
use yii\base\Model;

class GameSearch extends Model
{
    public $title = "";
    public $category;

    public function rules()
    {
        return [
            [['title', 'category'], 'safe'],
        ];
    }

}
