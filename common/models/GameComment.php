<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;

use Yii;

class GameComment extends \yii\db\ActiveRecord {
    
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName() {
        return 'game_comment';
    }

    public function rules() {
        return [
            [['comment'], 'required'],
            [['id', 'game_id', 'user_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'game_id' => Yii::t('app', 'Game ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    public function getGame() {
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function find() {
        return new GameCommentQuery(get_called_class());
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->user_id = Yii::$app->loggedin->user->id;
            return true;
        }
        return false;
    }
    
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

}
