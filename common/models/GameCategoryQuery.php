<?php

namespace common\models;

use Yii;

class GameCategoryQuery extends \yii\db\ActiveQuery
{
    public function listForSearch(){
        $data = [];
        $data[""]= Yii::t('app', 'All Categories');
        $model = $this->all();
        foreach($model as $row){
            $data[$row->id] = $row->name;
        }
        return $data;
    }
    
    public function getTotalGameCategory(){
        return $this->count();
    }
}