<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yiidreamteam\upload\ImageUploadBehavior;
use yii\data\ActiveDataProvider;

class Game extends \yii\db\ActiveRecord {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName() {
        return 'game';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 350, 'height' => 200],
                ],
                'filePath' => '@webroot/upload/game/[[pk]].[[extension]]',
                'fileUrl' => '/upload/game/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/upload/game/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/upload/game/[[profile]]_[[pk]].[[extension]]',
            ],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->user_id = Yii::$app->loggedin->user->id;
            return true;
        }
        return false;
    }

    public function rules() {
        return [
            [['title', 'description'], 'required'],
            [['user_id', 'category_id'], 'integer'],
            [['title', 'created_at', 'updated_at', 'image'], 'safe'],
            [['slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['image', 'file', 'extensions' => 'jpeg, jpg'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'created' => Yii::t('app', 'Created'),
            'image' => Yii::t('app', 'Image'),
            'user_id' => Yii::t('app', 'User'),
            'category_id' => Yii::t('app', 'Category'),
            'category.name' => Yii::t('app', 'Category'),
            'created_at' => Yii::t('app', 'Created Date'),
            'updated_at' => Yii::t('app', 'Updated Date'),
        ];
    }

    public function getCategory() {
        return $this->hasOne(GameCategory::className(), ['id' => 'category_id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getGameComments() {
        return $this->hasMany(GameComment::className(), ['game_id' => 'id']);
    }

    public function getGameCommentActive() {
        return $this->hasMany(GameComment::className(), ['game_id' => 'id'])->where('status=1');
    }

    public static function find() {
        return new GameQuery(get_called_class());
    }

    public function getCategoryList() {
        $model = GameCategory::find()->all();
        return ArrayHelper::map($model, 'id', 'name');
    }

    public function getDescShort() {
        return substr(strip_tags($this->description), 0, 300) . '...';
    }

    public function getLinkDetail() {
        return Yii::$app->urlManager->createUrl(['game/view', 'slug' => $this->slug]);
    }

    public function getLinkDetailFrontend() {
        return Yii::$app->urlManagerFrontEnd->createUrl(['game/view', 'slug' => $this->slug]);
    }

    public function getTotalComments() {
        return count($this->gameCommentActive);
    }

    public function getCommentDataProvider() {
        $model = GameComment::find()->gameId($this->id)->active();
        return new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ],
        ]);
    }

}
