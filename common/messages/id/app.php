<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'English' => 'Bahasa Inggris',
    'Indonesia' => 'Bahasa Indonesia',
    'Language' => 'Bahasa',
    '(Maximum image size is 3MB, and supported format are : "jpg", "jpeg", "png", "gif", "bmp".)' => 'Maksimum ukuran gambar 3mb, dan format harus : "jpg", "jpeg", "png", "gif", "bmp".',
    'Administrators' => 'Administrator',
    'All Categories' => 'Semua Kategori',
    'Category' => 'Kategori',
    'Change Password' => 'Ubah Kata Sandi',
    'Comment' => 'Komentar',
    'Create New Administrator' => 'Buat Admin Baru',
    'Create New Game' => 'Buat Game Baru',
    'Create New Member' => 'Buat Anggota Baru',
    'Create Profile' => 'Buat Profil',
    'Created' => 'Dibuat',
    'Created Date' => 'Dibuat Tanggal',
    'Description' => 'Deskripsi',
    'Find Title' => 'Pencarian Judul',
    'Game ID' => 'ID Permainan',
    'Games' => 'Permainan',
    'Home' => 'Beranda',
    'ID' => 'ID',
    'Image' => 'Gambar',
    'Login' => 'Masuk',
    'Logout' => 'Keluar',
    'Members' => 'Anggota',
    'My Account' => 'Akun Saya',
    'My Posts' => 'Postingan Saya',
    'Name' => 'Nama',
    'New ' => 'Baru',
    'New Password' => 'Kata Sandi Baru',
    'New Post' => 'Postingan Baru',
    'Old Password' => 'Kata Sandi Lama',
    'Online Game Portal' => 'Online Game Portal',
    'Post' => 'Postingan',
    'Repeat New Password' => 'Ulangi Kata Sandi Baru',
    'Search' => 'Cari',
    'Signup' => 'Daftar',
    'Slug' => 'Slug',
    'Title' => 'Judul',
    'Update Administrator' => 'Perbaharui Administrator',
    'Update Game' => 'Perbaharui Permainan',
    'Update Member' => 'Perbaharui Anggota',
    'Update Profile' => 'Perbaharui Profil',
    'Updated Date' => 'Diperbaharui Tanggal',
    'User' => 'Anggota',
    'User ID' => 'ID Anggota',
];
