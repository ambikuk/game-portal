<?php

namespace common\components\extentions;

use Yii;
use yii\base\Component;
use common\models\Currency;

class Loggedin extends Component {

    protected $user = false;
    protected $currency;

    public function init()
    {
        if (!Yii::$app->user->isGuest){
            $this->user = Yii::$app->user->identity;
        }
        return parent::init();
    }
    
    public function getUser(){
        return $this->user;
    }

}
