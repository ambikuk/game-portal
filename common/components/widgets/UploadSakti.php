<?php
/*
 * Name : Upload Sakti
 * Email : ambikuk@gmail.com
 */

namespace common\components\widgets;

use Yii;
use yii\base\Widget;

/*
 How To Use : 
 \common\components\widgets\UploadSakti::widget([
        'angular' => [
            'module' => 'module',
            'controller' => 'ControllerCtrl'
        ],
        'action' => [
            'load' => 'product/loadpicture/',
            'upload' => 'product/upload/'
        ]
    ]);
*/

class UploadSakti extends Widget {

    public $angular = [];
    public $action = [];

    public function run()
    {
        return $this->render('gmap-waypoints');
    }

}

?>