<?php
/*
 * Name : TimePicker
 * Desc : Widget for Timepiker
 * Author : sandi <sandi@ebizu.com>
 */

namespace common\components\widgets;


use yii;
use yii\base\Widget;

class ScheduleTimePicker extends Widget {
    public $element_name;
    public $field_to_fill;
	public $model;
    public $model_name;
	public $model_selected;
    public $restiric_field;
    public function run(){
        return $this->render('scheduletimepicker',[
            'element_name'      => $this->element_name,
            'field_to_fill'     => $this->field_to_fill,
            'model_name'        => $this->model_name,
            'restiric_field'    => $this->restiric_field,
			'model'             => $this->model,
			'model_selected' 	=> (object)$this->model_selected,
        ]);
    }

} 