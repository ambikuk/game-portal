<?php
/*
 * Name : TimePicker
 * Desc : Widget for Timepiker
 * Author : sandi <sandi@ebizu.com>
 */

namespace common\components\widgets;


use yii;
use yii\base\Widget;

class TimePicker extends Widget {
    public $element_name;
    public $field_to_fill;
	public $model;
    public function run(){
        return $this->render('timepicker',[
            'element_name' => $this->element_name,
            'field_to_fill' => $this->field_to_fill,
			'model' => $this->model,
        ]);
    }

} 