<?php
/*
 * Name : MyImageCropper
 * Desc : Widget for handling image cropping
 * Author : tajhul <tajhul@ebizu.com>
 * Copied from tajhul
 */

namespace common\components\widgets;

use yii;
use yii\base\Widget;

class MyImageCropper extends Widget {
    public $image_to_replace;
    public $field_to_fill;
    public $prepix = 'img_';
    public $modal_id;
    public $modal_title = 'Set the picture';
    public $href_btn_id;
    public $local_dir = null;
    public $server_dir = null;
    public $runtime_folder = null;
    public $app_dir = null;
    public $cdn_url = null;
    public $controller = null;
    public $upload_action = null;
    public $crop_action = null;
    public $skip_crop_action = null;
    public $save_action = null;
    public $wsmall = 180;
    public $hsmall = 78;
    public $wbig = 600;
    public $hbig = 260;
    public $ratio = 2.3;

    public function run(){
        return $this->render('mychangeimage',[

            // modal params
            'href_btn_id' => $this->href_btn_id,
            'image_to_replace' => $this->image_to_replace,
            'field_to_fill' => $this->field_to_fill,
            'prepix' => $this->prepix . time(),
            'modal_id' => $this->modal_id,
            'modal_title' => $this->modal_title,

            // image storage config params
            'local_dir' => $this->local_dir,
            'server_dir' => $this->server_dir,
            'runtime_folder' => $this->runtime_folder,
            'app_dir' => $this->app_dir,
            'cdn_url' => $this->cdn_url,

            // image operation config params
            'controller' => $this->controller,
            'upload_action' => $this->upload_action,
            'crop_action' => $this->crop_action,
            'skip_crop_action' => $this->skip_crop_action,
            'save_action' => $this->save_action,

            // image dimenstion config params
            'wsmall' => $this->wsmall,
            'hsmall' => $this->hsmall,
            'wbig' => $this->wbig,
            'hbig' => $this->hbig,
            'ratio' => $this->ratio,

        ]);
    }
}