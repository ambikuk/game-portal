<?php
/*
 * Name : Upload Sakti
 * Email : ambikuk@gmail.com
 */

namespace common\components\widgets;

use Yii;
use yii\base\Widget;

/*
 How To Use : 
 \common\components\widgets\UploadSakti::widget([
        'angular' => [
            'module' => 'module',
            'controller' => 'ControllerCtrl'
        ],
        'action' => [
            'load' => 'product/loadpicture/',
            'upload' => 'product/upload/'
        ]
    ]);
*/

class GmapWaypoints extends Widget {

    public $start = 'Halifax, NS';
    public $end = 'Vancouver, BC';

    public function run()
    {
        return $this->render('gmap-waypoints',[
            'start' => $this->start,
            'end' => $this->end
        ]);
    }

}

?>