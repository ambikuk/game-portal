<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;

    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var center = new google.maps.LatLng(-1.230374, 115.386658);
        var mapOptions = {
            zoom: 6,
            center: center
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        directionsDisplay.setMap(map);

        var start = document.getElementById('start').value;
        var end = document.getElementById('end').value;
        calcRoute(start, end);
    }

    function calcRoute(start, end) {
        var request = {
            origin: start,
            destination: end,
//            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
    #gmap-waypoints,#map-canvas{
        width: 100%;
        height: 100%;
    }
</style>
<div id="gmap-waypoints">
    <div id="map-form">
        <input type="hidden" name="start" value="<?= $start ?>" id="start" />
        <input type="hidden" name="end" value="<?= $end ?>" id="end" />
    </div>
    <div id="map-canvas"></div>
</div>