<div class="modal fade" id="picture" tabindex="-1" role="dialog" aria-labelledby="myAdsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ data.model.prod_name}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group clearfix">
                    <label class="col-sm-3 control-label" style="text-align:right">Name</label>
                    <div class="col-sm-6">
                        <input type="file" ng-file-select="onFileSelect($files)" >
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-sm-3 control-label" style="text-align:right">Preview</label>
                    <div class="col-sm-6">
                        <div id="preview">
                            <img src="{{ data.image}}" width="380px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- /.modal -->
</div>
<link href="<?= Yii::$app->homeUrl ?>plugins/loading-bar/css/loading-bar.css" rel="stylesheet">
<script src="<?= Yii::$app->homeUrl ?>js/angular.min.js"></script>
<script src="<?= Yii::$app->homeUrl ?>js/angular-touch.min.js"></script>
<script src="<?= Yii::$app->homeUrl ?>js/angular-animate.min.js"></script>
<script src="<?= Yii::$app->homeUrl ?>plugins/loading-bar/js/loading-bar.js"></script>
<script src="<?= Yii::$app->homeUrl ?>js/angular-file-upload-shim.js"></script> 
<script src="<?= Yii::$app->homeUrl ?>js/angular-file-upload.js"></script> 
<script>
                                var baseUrl = '<?= Yii::$app->homeUrl ?>';
                                var app = angular.module('<?= $angular['module'] ?>', ['angularFileUpload', 'ngTouch', 'chieffancypants.loadingBar', 'ngAnimate']);
                                app.controller('<?= $angular['controller'] ?>', function($scope, $upload, $http) {
                                    $scope.data = [];

                                    $scope.viewImage = function(id) {
                                        $http.get(baseUrl + '<?= $action['load'] ?>', {
                                            params: {id: id}
                                        }).success(function(data) {
                                            $scope.data = data;
                                            $('#picture').modal({show: true});
                                        });
                                        $scope.onFileSelect = function($files) {
                                            //$files: an array of files selected, each file has name, size, and type.
                                            for (var i = 0; i < $files.length; i++) {
                                                var file = $files[i];
                                                $scope.upload = $upload.upload({
                                                    url: baseUrl + '<?= $action['upload'] ?>',
                                                    data: {id: id},
                                                    file: file,
                                                }).progress(function(evt) {
                                                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                                                }).success(function(data, status, headers, config) {
                                                    $scope.data = data;
                                                    $('tr[data-key="' + id + '"] img').attr('src', data.image);
                                                });
                                            }
                                        }
                                    };
                                }).directive('errSrc', function() {
                                    var fallback = {
                                        link: function(scope, elem, attrs) {
                                            elem.bind('error', function() {
                                                angular.element(this).attr('src', attrs.errSrc);
                                            });
                                        }
                                    };
                                    return fallback;
                                }).config(function(cfpLoadingBarProvider) {
                                    cfpLoadingBarProvider.includeSpinner = false;
                                });
</script>