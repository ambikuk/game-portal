<?php
use yii\helpers\BaseUrl;
$FQDN = ''; // fully qulified domain name
//if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1')
    $FQDN = Yii::$app->homeUrl; // for local development purpose
?>

<?php $this->registerCssFile($this->theme->baseUrl . '/css/jcrop/jquery.Jcrop.css');?>
<?php $this->registerCssFile($this->theme->baseUrl . '/css/styles.css');?>
<?php $this->registerJsFile($this->theme->baseUrl . '/js/jcrop/jquery.Jcrop.js',['depends' => app\themes\avant\assets\AppAsset::className()]); ?>
<?php $this->registerJsFile($this->theme->baseUrl . '/js/jquery.form.js',['depends' => app\themes\avant\assets\AppAsset::className()]); ?>

<?php
$BaseURL = Yii::$app->homeUrl != '/' ? Yii::$app->homeUrl : BaseUrl::base(true) . '/';
$this->registerCss(".imgToCrop{ max-height: 300px; max-width: 300px; }");
$this->registerJs("
    var home_url = '". $BaseURL ."';
    var img_to_replace = '". $image_to_replace ."';
    var field_to_fill = '". $field_to_fill ."';
    var modal_id = '". $modal_id ."';
    var href_btn_id = '". $href_btn_id ."';

    var local_dir = '". $local_dir ."';
    var server_dir = '". $server_dir ."';
    var runtime_folder = '".$runtime_folder."';
    var app_dir = '".$app_dir."';
    var cdn_url = '". $cdn_url ."';

    var targetreplace = null;
    var name		= null;
    var type		= null;
    var wsmall		= null;
    var hsmall		= null;
    var wbig		= null;
    var hbig		= null;
    var ratio       = null;
    var xFactor     = null;
    var csrf        = null;

    $(document).ready(function()
    {
        $('#'+ href_btn_id).on('click', function(){
            $(modal_id).find('#popup-img').css({
                'height' : '305px'
            });
        });

        //initCrop();
        csrf = '". Yii::$app->request->csrfToken ."';
        var imgSrc = home_url + 'img/loader.gif';
        $(document).on('change', '#photoimg', function(){
            $('#preview').show();
            $('#spliter').html('<img src='+imgSrc+'>');

            /*
            * uploading image firstly
            */
            $('#imageform').ajaxForm(
            {
                //target: '#preview',
                beforeSubmit : function(formData, jqForm, options){
                    $('#img-to-crop').empty();
                },
                error : function(){
                    alert('Something wrong while uploading file, please try again!');
                    $('#popup-img').css({
                        'height' : '305px'
                    });
                    $('#img-to-crop').empty();
                    $('#preview').hide();
                },
                success : function(data){
                    if(data !== undefined) {
                        console.log(data);
                        initCrop();
                        data = JSON.parse(data);
                        if(data.error !== undefined){
                            alert(data.error);
                            $('#popup-img').css({
                                'height' : '305px'
                            });
                            $('#img-to-crop').empty();
                            $('#preview').hide();
                        } else {
                            $('#img-to-crop').append('<img id=cropbox class=imgToCrop src='+ baseUrl + data.imgSrc+'>');
                            $('#img-result').show();
                            $('#popup-img').css({
                                'height' : '530px'
                            });
                            $('#previmg').val(data.actual_image_name);

                            targetreplace = data.targetreplace;
                            name		= data.actual_image_name;
                            type		= data.type;
                            wsmall		= data.wsmall;
                            hsmall		= data.hsmall;
                            wbig		= data.wbig;
                            hbig		= data.hbig;
                            ratio       = data.ratio;
                            xFactor     = data.xFactor;

                            initCrop();
                        }
                    } else {
                        alert('Invalid data!');
                    }
                    $('#spliter').hide();
                }
            }).submit();
        });
    });

    function initCrop()
    {
        $('#cropbox').Jcrop({
            onChange:   updateCoords,
            aspectRatio: '". $ratio ."', // 1 = squere, 1.5, dst
            onSelect: updateCoords,
            //            boxWidth: 300,
            //            boxHeight: 300
        },function(){
            $('.jcrop-holdermain').parent().find('div:first').next().hide();
        });
    }

    function updateCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
        $('#crop_image_button').removeAttr('disabled');
    };

    function checkCoords()
    {
        if (parseInt($('#w').val())) return true;
        alert('Please select a crop region first.');
        return false;
    };

    function changeStep(temp)
    {
        switch (temp) {
            case 1:
                $('#step').html('STEP 1: Choose your picture');
                break;
            case 2:
                $('#step').html('STEP 2: Crop your picture, and save the changes');
            default:
                break;
        };
    }

    /* start crop */
    $('#crop_image_button').click(function()
    {
        var csrfToken 	= csrf;
        var url_post 	= home_url + '".$controller."/".$crop_action."/';
        var x			= $('#x').val()*xFactor;
        var y			= $('#y').val()*xFactor;
        var w			= $('#w').val()*xFactor;
        var h			= $('#h').val()*xFactor;
        var loader      = home_url + 'img/loading.gif';

        //return false;
        $('#crop_result').empty().html('<img src='+loader+'>');
        $.ajax({
            type: 'POST',
            url: url_post,
            data: {_csrf:csrfToken,name:name,targetreplace:targetreplace,type:type,wsmall:wsmall,hsmall:hsmall,wbig:wbig,hbig:hbig,x:x,y:y,w:w,h:h,server_dir:server_dir,local_dir:local_dir,cdn_url:cdn_url,app_dir:app_dir,runtime_folder:runtime_folder},
            dataType: 'json',
            success: function(data){
                $('#crop_result').empty().html(data.image);
                $('#button_save_picture').removeAttr('disabled');
                name = data.new_name;
            }
        }).done(function(){
            initCrop();
        });
    });

    /*
    * Skip Crop Image
    */
    $('#skip-crop-pic').click(function()
    {
        var csrfToken 	= csrf;
        var url_post 	= home_url + '".$controller."/".$skip_crop_action."/';;
        var loader      = home_url + 'img/loading.gif';

        $('#crop_result').empty().html('<img src='+loader+'>');
        $.ajax({
            type: 'POST',
            url: url_post,
            data: {_csrf:csrfToken,name:name,type:type,wsmall:wsmall,hsmall:hsmall,wbig:wbig,hbig:hbig,server_dir:server_dir,local_dir:local_dir,cdn_url:cdn_url,app_dir:app_dir,runtime_folder:runtime_folder},
            dataType: 'json',
            success: function(data){
                // console.log(data);
                $('#crop_result').html(data.image);
                $('#new_image').val(data.new_name);

                $('#image_url').val(data.image_url);
                $('#button_save_picture').removeAttr('disabled');

                //$('#crop_result').empty().html(data.image);
                //$('#button_save_picture').removeAttr('disabled');
                name = data.new_name;
            }
        });
    });

    /*
     * Saving Image
     * */
    $('#button_save_picture').on('click',function()
    {
        var csrfToken 	= csrf;
        var url_post 	= home_url + '".$controller."/".$save_action."/';;
        var type		= 'business';
        var section		= 'productimage';
        var id			= '';
        var targetreplace = targetreplace;

        $.ajax({
            type: 'POST',
            url: url_post,
            data: {_csrf:csrfToken, name:name, type:type, section:section, id:id,targetreplace:targetreplace,server_dir:server_dir,local_dir:local_dir,cdn_url:cdn_url,app_dir:app_dir,runtime_folder:runtime_folder},
            beforeSend : function(){
                $('#spliter-text').text('Saving Picture, Please wait ...').show();
            },
            success: function(response){
                if(response !== undefined){
                    var data = JSON.parse(response);
                    if(data.success == 1){
                        $('#preview').hide();
                        $('#crop_result').empty();
                        $('#img-to-crop').empty();
                        window.parent.$(modal_id).modal('hide');
                        window.parent.$(img_to_replace).attr('src', data.cdn_img);
                        window.parent.$(field_to_fill).val(data.img_name);
                    }
                }
            }
        }).done(function(){
            $('#spliter-text').hide();
        });
    });
", \yii\web\View::POS_READY, 'image-options');
?>

<!--start modal-->
<div class="modal fade" id="<?=str_replace('#','', $modal_id)?>" tabindex="-1" role="dialog" aria-labelledby="myUserLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="">
        <div id="popup-img" class="modal-content" style="height:305px;width: 720px;">

            <?php $name='product_'.time();?>

            <div id="modal_change_picture_voucher">
                <div class="modal-header" style="">
                    <button type="button" id="button_close_picture" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><i class="fa fa-picture-o"></i> <?=Yii::t('app',$modal_title)?></h4>
                </div>

                <div class="modal-body">
                    <div id="step" class="subtitle_green">
                        STEP 1: Choose Your Picture
                    </div>

                    <form id="imageform" method="post" enctype="multipart/form-data" action='<?=$FQDN?><?=$controller?>/<?=$upload_action?>/'>
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken?>" />
                        <input type="hidden" name="targetreplace" id="targetreplace" value="<?php echo isset($targetreplace)
                        ? $targetreplace : ""?>" />
                        <input type="hidden" id="local_dir" name="local_dir" value="<?=$local_dir?>" />
                        <input type="hidden" id="server_dir" name="server_dir" value="<?=$server_dir?>" />
                        <input type="hidden" id="runtime_folder" name="runtime_folder" value="<?=$runtime_folder?>" />
                        <input type="hidden" id="app_dir" name="app_dir" value="<?=$app_dir?>" />
                        <input type="hidden" id="cdn_url" name="cdn_url" value="<?=$cdn_url?>" />
                        <input type="hidden" name="previmg" id="previmg" value="" />
                        <input type="file" name="photoimg" id="photoimg" />
                        <input type="hidden" name="name" value="<?php echo $prepix?>" />
                        <input type="hidden" name="new_image" id="new_image" value="" />
                        <input type="hidden" name="image_url" id="image_url" value="" />
                        <input type="hidden" name="type" value="business" />
                        <input type="hidden" name="wsmall" value="<?=$wsmall?>" />
                        <input type="hidden" name="hsmall" value="<?=$hsmall?>" />
                        <input type="hidden" name="wbig" value="<?=$wbig?>" />
                        <input type="hidden" name="hbig" value="<?=$hbig?>" />
                        <input type="hidden" name="ratio" value="<?=$ratio?>" />
                        <span class="smalltext_darkgray"><small><?=Yii::t('app','(Maximum image size is 3MB, and supported format are : "jpg", "jpeg", "png", "gif", "bmp".)')?></small></span>
                    </form>

                    <div id='preview'>
                        <div id="spliter"></div>
                        <div id="img-result" style="display: none;">
                            <div class="col-xs-12">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr valign="top">
                                        <td style="width: 300px;"><span class="subsubtitle_blue">
                                            <?php echo 'Actual Image, Dimension : '?></span>
                                        </td>
                                        <td style="padding-left: 20px;"><span class="subsubtitle_blue"><?php echo 'Crop Result';?></span></td>
                                    </tr>
                                    <tr valign="middle">
                                        <td id="img-to-crop" style="width: 300px; height: 300px; border: 1px dotted #999;vertical-align: middle;">
                                        </td>
                                        <td style="padding-left: 20px;">
                                            <div style="width: 300px; height: 300px; border: 1px dotted #999; display: table-cell; vertical-align: middle;">
                                                <div id="crop_result" style="text-align: center;"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-xs-12" style="margin-top: 15px; margin-bottom: 0px;">
                                <form action="crop.php" method="post" onsubmit="return checkCoords();" style="">
                                    <input type="hidden" id="local_dir2" name="local_dir" value="<?=$local_dir?>" />
                                    <input type="hidden" id="server_dir2" name="server_dir" value="<?=$server_dir?>" />
                                    <input type="hidden" id="runtime_folder2" name="runtime_folder" value="<?=$runtime_folder?>" />
                                    <input type="hidden" id="app_dir2" name="app_dir" value="<?=$app_dir?>" />
                                    <input type="hidden" id="cdn_url2" name="cdn_url" value="<?=$cdn_url?>" />
                                    <input type="hidden" id="x" name="x" value="" />
                                    <input type="hidden" id="y" name="y" value=""/>
                                    <input type="hidden" id="w" name="w" value=""/>
                                    <input type="hidden" id="h" name="h" value=""/>
                                    <div class="col-xs-5">
                                        <input disabled="disabled" type="button" value="Crop Image" class="btn btn-primary" id="crop_image_button" style="margin-left:5px;cursor: pointer;" />
                                        <span class="btn btn-primary" style="cursor: pointer;" id="skip-crop-pic">Skip crop</span>
                                    </div>
                                    <div class="col-xs-4" style="text-align: right;">
                                        <span id="spliter-text" style="position: relative;top:7px;display: none;"></span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="button" id="button_back_picture" class="pull-right btn btn-inverse" value="Back" style="display: none;" />
                                        <input type="button" id="button_save_picture" class="pull-right btn btn-primary" value="Save" disabled="" style="margin-right: 5px;"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- end modal -->
