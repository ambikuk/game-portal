<?php
$fieldStart 	= 'com_open_'.$element_name;
$fieldEnd 		= 'com_close_'.$element_name;
$fieldStatus 	= 'com_status_'.$element_name;
$timeStart 		= substr($model->$fieldStart,0,5);
$timeEnd 		= substr($model->$fieldEnd,0,5);
$status 		= $model->$fieldStatus;
$arrStatus 		= array('0'=>'Close','1'=>'Open 24 hour','2'=>'Open limited time');

$disabled = 'as';
if($status==0 || $status==1){
	$disabled = 'disabled="disabled"';
}

?>
<div class="form-group">
    <label class="col-lg-3 control-label">&nbsp;</label>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="col-lg-6">
                <label class="col-lg-4 control-label"><?php echo ucfirst($element_name)?></label>
                <div class="col-lg-8">
                    <select class="form-control opening_status" id="select_<?=$element_name;?>" name="Company[<?=$fieldStatus;?>]" onChange="setOpeningstatus('<?=$element_name;?>',this)">
                        <?php foreach($arrStatus as $key => $value){
							if($model->$fieldStatus == $key){
							?>
                        	<option value="<?=$key;?>" selected><?=$value;?></option>
                        <?php }else{ ?>
                        	<option value="<?=$key;?>"><?=$value;?></option>
                        <?php } 
						}?>
                    </select>
                </div>
                
                
            </div>
            <div class="col-lg-6">
                <label class="col-lg-2 control-label">Time</label>
                <div class="col-lg-4">
                	<input type="text" class="form-control element_timepicker timeStart" id="<?=$fieldStart;?>" <?=$disabled;?> name="Company[<?=$fieldStart;?>]" placeholder="00:00" value="<?=$timeStart;?>">
                </div>
                <label class="col-lg-2 control-label">To</label>
                <div class="col-lg-4">
                	<input type="text" class="form-control element_timepicker timeEnd" id="<?=$fieldEnd;?>" <?=$disabled;?> name="Company[<?=$fieldEnd;?>]" placeholder="00:00" value="<?=$timeEnd;?>" >
                 </div>
            </div>
        </div>
    </div>
</div>