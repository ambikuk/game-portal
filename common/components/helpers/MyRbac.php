<?php

namespace common\components\helpers;

use Yii;
use yii\base\Event;

class MyRbac {

    public static function level($rights = [], $type, $label, $link, $options = [])
    {
        $html = '';

        switch ($type) {
            case 'link':
                $html = Html::a($label, [$link[0]], $options);
                break;
            case 'child_menu':
                $html = '<li><a href="'.Yii::$app->urlManager->createUrl($link[0]).'">'.$label.'</a></li>';
                break;
            case 'big_menu':
                $html = '<li class="divider"></li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-frown-o"></i> <span> '.$label.'</span> </a>
                        <ul class="acc-menu">
                ';
                foreach($link as $k => $v)
                    $html .= '<li><a href="'.Yii::$app->urlManager->createUrl($v).'">'.$k.'</a></li>';
                $html .= '
                        </ul>
                    </li>';
                break;
            case 'popup':
                $str = explode(', ', $label);
                $html = Html::a($str[0], $link, [
                    'title' => Yii::t('yii', $str[1]),
                    'data-confirm' => Yii::t('yii', 'Are you sure would like to '.$str[2].'?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'style' => 'margin: 0 2px',
                ]);
                break;
            case 'delete':
                $html = Html::a('<i class="fa fa-trash-o"></i>', [$link[0]], [
                    'title' => Yii::t('yii', $label),
                    'data-confirm' => Yii::t('yii', 'Are you sure would like to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'style' => 'margin: 0 2px',
                ]);
                break;
            case 'delete-bg':
                $html = Html::a('<i class="fa fa-trash-o"></i> '.$label, [$link[0]], [
                    'title' => Yii::t('yii', $label),
                    'data-confirm' => Yii::t('yii', 'Are you sure would like to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'style' => 'margin: 0 2px',
                    'class' => 'btn btn-danger'
                ]);
                break;
        }
        if(Yii::$app->user->identity->type == 1) {
            if($rights[0] == 1) {
                if(Yii::$app->user->identity->level == 1) {
                    return $html;
                }
            }
            if($rights[1] == 1) {
                if(Yii::$app->user->identity->level == 2 || Yii::$app->user->identity->level == 1) {
                    return $html;
                }
            }
        }
    }

}
