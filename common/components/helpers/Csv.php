<?php

namespace common\components\helpers;

class Csv {

    public function __construct($data)
    {
        $this->process($data);
    }

    protected function process($data)
    {
        var_dump($this->csv_to_array($data->tempName, "r"));
        exit;
    }

    protected function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                $data[] = $row;
            }
            var_dump($data);
            exit;
            fclose($handle);
        }
        return $data;
    }

    public static function read($data)
    {
        return new Csv($data);
    }

}
