<?php

namespace common\components\helpers;

use Yii;
use common\components\helpers\S3;

class JcropAws {

    protected $sizes = [];
    protected $quality = 90;
    protected $source;
    protected $coor = [];
    protected $path;
    protected $dirTemp = 'temp';
    protected $baseUrl;
    protected $name;

    public function create($size)
    {
        $targetWeight = $targetHeight = $size;
        
        $source = imagecreatefromjpeg($this->source);
        $image = ImageCreateTrueColor($targetWeight, $targetHeight);
        imagecopyresampled($image, $source, 0, 0, $this->coor['x'], $this->coor['y'], $targetWeight, $targetHeight, $this->coor['w'], $this->coor['h']);

        $dir = $this->dirTemp . '/' . $size.'/';
        $nameFile = str_replace($this->path.'/original/', '', $this->name);
        $dirTemp = $dir.$nameFile;
        
        $awsFile = str_replace('/original', '/'.$size, $this->name);
       
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        
        imagejpeg($image, $dirTemp, $this->quality);
       
        s3::Upload($awsFile, $dirTemp);
        
        return $awsFile;
    }

    public function each()
    {
        $data = [];
        foreach ($this->sizes as $row) {
            $data[$row] = $this->create($row);
        }
        return $data;
    }

    public static function install($options = [])
    {
        $obj = new self;
        foreach ($options as $key => $val) {
            if (property_exists($obj, $key)) {
                $obj->$key = $val;
            }
        }
        return $obj->each();
    }

}
