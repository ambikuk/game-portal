<?php

namespace common\components\helpers;

use Yii;

class Identity {

	public static function menu()
	{
		$out = '';
		if(Yii::$app->user->identity->type == 3) {
			$out = '_MALL';
		} elseif(Yii::$app->user->identity->type == 2) {
			$out = '_TM';
		} else {
			$out = '_ADM';
		}
		return $out;
	}

	public static function WhoIs($role = 0)
	{
		$out = '';
		if($role == 0) {
			if(Yii::$app->user->identity->type == 3) {
				$out = Yii::$app->user->identity->mal_name;
			} elseif(Yii::$app->user->identity->type == 2) {
				$out = 'Telekom Malaysia';
			} else {
				$out = Yii::$app->user->identity->username;
			}
		} else {
			if(Yii::$app->user->identity->type == 3) {
				$out = 'mall';
			} elseif(Yii::$app->user->identity->type == 2) {
				$out = 'partner';
			} else {
				$out = 'admin';
			}
		}
		return $out;
	}

	public static function Query()
	{
		$out = '';
		if(Yii::$app->user->identity->type == 1) {
			$out = "AND `com_type` = 0 AND `com_hq_id` = 0 AND `com_registered_to` = 'EBC'";
		} elseif(Yii::$app->user->identity->type == 2) {
			$out = "AND `com_type` = 0 AND `com_hq_id` = 0 AND `com_registered_to` = 'TM'";
		} else {
			$out = "AND `com_registered_to` = 'EBC' AND `com_in_mall` = 1 AND `com_type` = 0 AND `com_hq_id` = 0 AND `tbl_mall_merchant`.`mam_mal_id` = ".Yii::$app->user->identity->mall;
		}
		return $out;
	}

	public static function Join($table = '', $value = [])
	{
		$out = '';
		$out = "LEFT JOIN `".$table."` ON `".$table."`.`".$value[0]."` = `".$value[1]."` ";
		return $out;
	}

}
