<?php

namespace common\components\helpers;

class Jcrop {

    protected $sizes = [];
    protected $quality = 90;
    protected $image;
    protected $coor = [];
    protected $path;
    protected $baseUrl;
    protected $name;

    public function create($size, $ratioW = 1, $ratioH = 1)
    {
        $targ_w = $size;
        $targ_h = $size * $ratioH / $ratioW;

        $img_r = imagecreatefromjpeg($this->image);
        $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $this->coor['x'], $this->coor['y'], $targ_w, $targ_h, $this->coor['w'], $this->coor['h']);
        $name = str_replace($this->path . 'original/', '', $this->name);
        $dir = $this->baseUrl . $this->path . $size;
        $file = $dir . '/' . $name;
        $fileSave = $this->path . $size . '/' . $name;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        imagejpeg($dst_r, $file, $this->quality);

        return $fileSave;
    }

    public function each()
    {
        $data = [];
        foreach ($this->sizes as $key => $val) {
            $data[$key] = $this->create($key, $val['ratioW'], $val['ratioH']);
        }
        return $data;
    }

    public static function install($options = [])
    {
        $obj = new self;
        foreach ($options as $key => $val) {
            if (property_exists($obj, $key)) {
                $obj->$key = $val;
            }
        }
        return $obj->each();
    }

}
