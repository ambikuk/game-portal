<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\Expression;
use yii\db\ActiveRecord;
use common\models\ShipmentMeta;
use Aws\S3\S3Client;
use PHPImageWorkshop\ImageWorkshop;
use common\components\helpers\S3;

require_once Yii::$app->params['libPath'] . 'aws' . DIRECTORY_SEPARATOR . 'aws.phar';
require_once Yii::$app->params['libPath'] . 'PHPImageWorkshop' . DIRECTORY_SEPARATOR . 'ImageWorkshop.php';

class S3OriginalBehavior extends Behavior {

    public $field = 'image';
    public $field_crop = null;
    public $path = 'images';
    public $format = array("jpg", "jpeg", "png", "gif", "bmp");
    public $max = 3;

    public function events()
    {
        $data[ActiveRecord::EVENT_BEFORE_INSERT] = 's3Function';
        $data[ActiveRecord::EVENT_BEFORE_UPDATE] = 's3Function';
        $data[ActiveRecord::EVENT_BEFORE_DELETE] = 's3Delete';
        return $data;
    }

    protected function deleteCrops()
    {
        $attr = $this->field_crop;
        if (!empty($this->owner->$attr)) {
            $attrs = unserialize($this->owner->$attr);
            foreach ($attrs as $row) {
                S3::Delete($row);
            }
        }
    }

    public function s3Delete($event)
    {
        if (!is_null($this->field_crop))
            $this->deleteCrops();

        $attr = $this->field;
        S3::Delete($this->owner->$attr);
    }

    public function s3Function($event)
    {
        $attr = $this->field;
        if (!$this->owner->isNewRecord)
            $oldAttr = '/' . $this->path . '/' . $this->owner->oldAttributes[$attr];
        if (isset($this->owner->$attr) && is_object($this->owner->$attr)) {
            $fileImage = $this->owner->$attr;
            if ($fileImage->name !== "") {
                list($txt, $ext) = explode(".", $fileImage->name);

                if (in_array(strtolower($ext), $this->format)) {
                    if ($fileImage->size < (1024 * 1024 * $this->max)) {
                        $newName = strtolower($txt) . '-' . substr(md5(time()), 0, 10) . "." . strtolower($ext);
                        $actualImageName = '/' . $this->path . '/' . $newName;
                        $actualImageName = strtolower(str_replace(' ', '-', $actualImageName));

                        if (!$this->owner->isNewRecord)
                            S3::Delete($oldAttr);
                        
                        S3::Upload($actualImageName, $fileImage->tempName);

                        $this->owner->$attr = $newName;
                    } else
                        $this->owner->addError($this->field, "Image file size max {$this->max} MB");
                } else
                    $this->owner->addError($this->field, 'Invalid file format..');
            } else
                $this->owner->$attr = $this->owner->oldAttributes[$attr];
        } else
            $this->owner->$attr = $this->owner->oldAttributes[$attr];
    }

}
