<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\Expression;
use yii\db\ActiveRecord;
use common\models\LogMasterChanges;

class LogMasterChangesBehavior extends Behavior {
    
    public $primaryKey;

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'create',
            ActiveRecord::EVENT_AFTER_UPDATE => 'update',
            ActiveRecord::EVENT_AFTER_DELETE => 'delete',
        ];
    }

    public function create($event) {
        LogMasterChanges::create($this->owner->tableSchema->name, $this->owner->attributes[$this->primaryKey], LogMasterChanges::ACTION_CREATE);
    }
    
    public function update($event) {
        LogMasterChanges::create($this->owner->tableSchema->name, $this->owner->attributes[$this->primaryKey], LogMasterChanges::ACTION_UPDATE);
    }
    
    public function delete($event) {
        LogMasterChanges::create($this->owner->tableSchema->name, $this->owner->attributes[$this->primaryKey], LogMasterChanges::ACTION_UPDATE);
    }

}
