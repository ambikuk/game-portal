<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\Expression;
use yii\db\ActiveRecord;
use Aws\S3\S3Client;
use PHPImageWorkshop\ImageWorkshop;
use common\components\helpers\S3;

require_once Yii::$app->basePath . '/../common/lib/' . 'aws' . DIRECTORY_SEPARATOR . 'aws.phar';
require_once Yii::$app->basePath . '/../common/lib/' . 'PHPImageWorkshop' . DIRECTORY_SEPARATOR . 'ImageWorkshop.php';

class S3UploadCitroBehavior extends Behavior {

    public $field = 'image';
    public $path = 'images';
    public $format = array("jpg", "jpeg", "png", "gif", "bmp");
    public $max = 3;

    public function events() {
        $data[ActiveRecord::EVENT_BEFORE_INSERT] = 's3Function';
        $data[ActiveRecord::EVENT_BEFORE_UPDATE] = 's3Function';
        $data[ActiveRecord::EVENT_BEFORE_DELETE] = 's3Delete';
        return $data;
    }

    public function s3Delete($event) {
        $this->deleteCrops();

        $attr = $this->field;
        S3::Delete($this->owner->$attr);
    }

    public function s3Function($event) {
        $attr = $this->field;
        if (isset($this->owner->$attr) && is_object($this->owner->$attr)) {
            $fileImage = $this->owner->$attr;
            if ($fileImage->name !== "") {
                list($txt, $ext) = explode(".", $fileImage->name);
                if (in_array(strtolower($ext), $this->format)) {
                    if ($fileImage->size < (1024 * 1024 * $this->max)) {
                        $newName = 'ad_' . $this->owner->id;
                        $actualImageName = '/' . $this->path . '/' . $newName . "." . strtolower($ext);

                        S3::Upload($actualImageName, $fileImage->tempName);

                        $this->owner->$attr = $actualImageName;
                    } else
                        $this->owner->addError($this->field, "Image file size max {$this->max} MB");
                } else
                    $this->owner->addError($this->field, 'Invalid file format..');
            }else {
                $this->owner->$attr = $this->owner->oldAttributes[$attr];
            }
        }
    }

}
