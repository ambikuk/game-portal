<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\Expression;
use yii\db\ActiveRecord;
use common\models\ChangeLog;
use common\models\User;

class UrldecodeBehavior extends Behavior {
    
    public $items = [];

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    public function afterFind($event) {
        foreach($this->items as $row){
            $this->owner->$row = urldecode($this->owner->$row);
        }
    }

}
