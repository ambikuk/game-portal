<?php

namespace common\components\behaviors;

use yii\base\Behavior;
use yii\base\Event;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\base\Exception;
use yii\behaviors\Inflector;

class SluggableBehavior extends Behavior {

    public $attributes = [
        ActiveRecord::EVENT_BEFORE_INSERT => 'title',
        ActiveRecord::EVENT_BEFORE_UPDATE => 'title',
    ];
    public $columns = [];
    public $fieldSlug = 'slug';
    public $unique = true;
    public $update = true;
    protected $_defaultColumnsToCheck = array('name', 'title');

    public function events()
    {

        $events = $this->attributes;
        foreach ($events as $i => $event) {
            $events[$i] = 'UpdateSlug';
        }
        return $events;
    }

    public function UpdateSlug($event)
    {
        $fildSlug = $this->fieldSlug;
        // Slug already created and no updated needed
        if (true !== $this->update && !empty($this->owner->slug)) {
            Yii::trace(
                    'Slug found - no update needed.', __CLASS__ . '::' . __FUNCTION__
            );
            return parent::beforeSave($event);
        }

        if (!is_array($this->columns)) {
            Yii::trace(
                    'Columns are not defined as array', __CLASS__ . '::' . __FUNCTION__
            );
            throw new Exception('Columns have to be in array format.');
        }

        $availableColumns = array_keys($this->owner->tableSchema->columns);

        // Try to guess the right columns
        if (0 === count($this->columns)) {
            $this->columns = array_intersect(
                    $this->_defaultColumnsToCheck, $availableColumns
            );
        } else {
            // Unknown columns on board?
            foreach ($this->columns as $col) {
                if (!in_array($col, $availableColumns)) {
                    if (false !== strpos($col, '.')) {
                        Yii::trace(
                                'Dependencies to related models found', __CLASS__ . '::' . __FUNCTION__
                        );
                        list($model, $attribute) = explode('.', $col);
                        $externalColumns = array_keys(
                                $this->getOwner()->$model->tableSchema->columns
                        );
                        if (!in_array($attribute, $externalColumns)) {
                            throw new Exception(
                            "Model $model does not haz $attribute"
                            );
                        }
                    } else {
                        throw new Exception(
                        'Unable to build slug, column ' . $col . ' not found.'
                        );
                    }
                }
            }
        }

        // No columns to build a slug?
        if (0 === count($this->columns)) {
            throw new Exception(
            'You must define "columns" to your sluggable behavior.'
            );
        }

        // Fetch values
        $values = array();
        foreach ($this->columns as $col) {
            if (false === strpos($col, '.')) {
                $values[] = $this->owner->$col;
            } else {
                list($model, $attribute) = explode('.', $col);
                $values[] = $this->getOwner()->$model->$attribute;
            }
        }

        // First version of slug
        $slug = $checkslug = Inflector::urlize(
                        implode('-', $values)
        );

        // Check if slug has to be unique
        if (false === $this->unique || (!$this->owner->isNewRecord && $this->owner->$fildSlug === $slug)
        ) {
            $this->owner->$fildSlug = $slug;
        } else {
            $counter = 0;
            while ($this->owner->findAll(
                    array($fildSlug => $checkslug))
            ) {
                $checkslug = sprintf('%s-%d', $slug, ++$counter);
            }
            $this->owner->$fildSlug = $counter > 0 ? $checkslug : $slug;
        }
    }

}
