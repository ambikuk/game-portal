<?php

namespace common\components\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\di\Instance;
use yii\web\User;
use yii\web\ForbiddenHttpException;
use common\models\UserPermission;

class PermissionFilter extends ActionFilter {

    public function init() {
        parent::init();
        $action = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        $model = UserPermission::find()->where([
            'action' => $action,
            'group_id' => Yii::$app->loggedin->user->ebu_pg_id,
            'available' => 0
        ]);
        if($model->count())
             throw new ForbiddenHttpException;   
        return true;
    }

}
