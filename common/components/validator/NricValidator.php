<?php

namespace common\components\validator;

use Yii;
use yii\validators\Validator;
use yii\web\JsExpression;
use yii\validators\ValidationAsset;
use yii\helpers\Json;

class NricValidator extends Validator
{
    public $pattern = '/^\d{6}-\d{2}-\d{4}$/';
    public $fullPattern = '/^[^@]*<[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?>$/';
    public $allowName = false;
    public $checkDNS = false;
    public $enableIDN = false;
    public function init()
    {
        parent::init();
        if ($this->enableIDN && !function_exists('idn_to_ascii')) {
            throw new InvalidConfigException('In order to use IDN validation intl extension must be installed and enabled.');
        }
        if ($this->message === null) {
            $this->message = Yii::t('yii', '{attribute} is not a valid nric must xxxxxx-xx-xxxx.');
        }
    }

   protected function validateValue($value)
    {
        // make sure string length is limited to avoid DOS attacks
        if (!preg_match('/^\d{6}-\d{2}-\d{4}$/', $value, $matches)) {
            $valid = false;
        } else {
            $valid = true;
        }

        return $valid ? null : [$this->message, []];
    }
    
   public function clientValidateAttribute($model, $attribute, $view)
    {
        $options = [
            'pattern' => new JsExpression($this->pattern),
            'fullPattern' => new JsExpression($this->fullPattern),
            'allowName' => $this->allowName,
            'message' => Yii::$app->getI18n()->format($this->message, [
                'attribute' => $model->getAttributeLabel($attribute),
            ], Yii::$app->language),
            'enableIDN' => (boolean) $this->enableIDN,
        ];
        if ($this->skipOnEmpty) {
            $options['skipOnEmpty'] = 1;
        }

        ValidationAsset::register($view);
        if ($this->enableIDN) {
            PunycodeAsset::register($view);
        }

        return 'yii.validation.nric(value, messages, ' . Json::encode($options) . ');';
    }
}
