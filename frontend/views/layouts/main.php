<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::t('app', 'Online Game Portal'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top bg-navy color-palette',
                ],
            ]);
            $menuItems = [
                ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']];
                $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
            } else {
                $menuItems[] = ['label' => Yii::t('app', 'Post'), 'items' => [
                        ['label' => Yii::t('app', 'New Post'), 'url' => ['/game/post']],
                        ['label' => Yii::t('app', 'My Posts'), 'url' => ['/game/index']]
                ]];
                $menuItems[] = ['label' => Yii::t('app', 'My Account'), 'items' => [
                        ['label' => Yii::t('app', 'Update Profile'), 'url' => ['/account/update']],
                        ['label' => Yii::t('app', 'Change Password'), 'url' => ['/account/changepassword']]
                ]];
                $menuItems[] = ['label' => Yii::t('app', 'Language'), 'items' => [
                        ['label' => Yii::t('app', 'Indonesia'), 'url' => ['?lang=id']],
                        ['label' => Yii::t('app', 'English'), 'url' => ['?lang=en_US']]
                ]];
                $menuItems[] = [
                    'label' => Yii::t('app', 'Logout').'(' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Buhori Dermawan <?= date('Y') ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
