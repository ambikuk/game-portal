<div class="row item-comment">
    <div class="col-lg-12">
        <div>
        <small>- <?= Yii::$app->formatter->asDatetime($model->created_at) ?> <cite title="<?= $model->user->username ?>"><?= $model->user->fullname ?></cite></small>
        </div>
        <?= $model->comment ?>
    </div>
</div>
