<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Games');
?>
<div class="row">
    <div class="col-md-12"><p class="pull-left"><?= Html::a('<i class="fa fa-plus"></i> <span>' . Yii::t('app', 'New ' . $this->title) . '</span>', ['post'], ['class' => 'btn btn-default DTTT_button_text']) ?></p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-body">
                <?=
                GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model,
                            ]),
                    'columns' => [
                        'created_at:datetime',
                        'updated_at:datetime',
                        'category.name',
                        'title',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '<span class="pull-right">{view} {update} {delete}</span>',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $model->linkDetail, array_merge([
                                        'title' => Yii::t('yii', 'Beacons'),
                                        'data-pjax' => '0',
                            ]));
                        }]
                        ]
                    ],
                    'tableOptions' => ['class' => 'table table-striped']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>