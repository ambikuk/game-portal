<?php

use yii\widgets\ListView;
use common\components\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GameCategory;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Online Game Portal');
?>

<!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => '/']); ?>
        <div class="col-lg-2">
            <?= Html::dropDownList('category', $category, GameCategory::find()->listForSearch(), ['class' => 'form-control']) ?>
        </div>
        <div class="col-lg-9">
            <?= Html::input('text', 'search', $search, ['class' => 'form-control','placeholder'=>Yii::t('app', 'Find Title')]) ?>
        </div>
        <div class="col-lg-1">
            <button class="btn-primary btn searchButton"><i class="fa fa-search"></i> <?= Yii::t('app', 'Search'); ?></button>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- /.row -->

<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list',
]);
?>
