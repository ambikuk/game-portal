<?php

use common\components\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

$this->title = ($model->isNewRecord ? Yii::t('app', 'Create New Game') : Yii::t('app', 'Update Game'));
?>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'title') ?>
                <?= $form->field($model, 'category_id')->dropDownList($model->categoryList) ?>
                <?=
                $form->field($model, 'description')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6],
                    'language' => 'en_CA',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]);
                ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-4">
                            <!-- Thumb 1 (thumb profile) -->
                            <?= Html::img($model->getThumbFileUrl('image', 'thumb', '/upload/game/default.jpg'), ['class' => 'img-thumbnail']) ?>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="btn-toolbar">
                            <button class="btn-primary btn"><i class="fa fa-check"></i> Post</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>
