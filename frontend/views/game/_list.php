<div class="col-md-4 portfolio-item">
    <a href="<?= $model->linkDetail ?>">
        <img class="img-responsive" src="<?= $model->getThumbFileUrl('image', 'thumb', '/upload/game/default.jpg') ?>" alt="">
    </a>
    <h3 class="box-title">
        <a href="<?= $model->linkDetail ?>"><?= $model->title ?></a>
    </h3>
    <div>
        <small>- <?= Yii::$app->formatter->asDatetime($model->created_at) ?> <cite title="<?= $model->user->username ?>"><?= $model->user->fullname ?></cite></small>
        <span class="pull-right">Comments(<?= $model->totalComments ?>)</span>
    </div>
    <p><?= $model->descShort ?></p>
</div>
