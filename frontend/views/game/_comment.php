<?php

use common\components\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
?>
<?php
$form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal'
            ],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Comment') ?></h3>
            </div>
            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="box-body">
                    <?= $form->field($comment, 'comment')->textarea() ?>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-1">
                            <div class="btn-toolbar">
                                <button class="btn-primary btn"><i class="fa fa-check"></i> Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="box-body">
                <?php
                echo ListView::widget([
                    'dataProvider' => $model->commentDataProvider,
                    'itemView' => '_comment_list',
                    'emptyText' => 'No comments found.'
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>
