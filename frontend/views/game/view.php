<?php

use yii\helpers\Html;

$this->title = $model->title;
?>
<section class="content-header">
    <h1>
        <?= $this->title ?>
        <small><?= $model->category->name ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= Yii::$app->getHomeUrl() ?>">Home</a></li>
        <li><a href="#"><?= $model->category->name ?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
            <?php if ($model->getImageFileUrl('image', 'empty') != 'empty'): ?>
                <div class="box-header with-border">
                    <?= Html::img($model->getImageFileUrl('image', '/upload/game/default.png')) ?>
                </div>
            <?php endif; ?>
            <div class="box-body">
                <?= $model->description ?>
            </div>
        <div class="box-body">
            <?php echo Yii::$app->controller->renderPartial('_comment', ['model' => $model, 'comment' => $comment]) ?>
        </div>
    </div>
</section>