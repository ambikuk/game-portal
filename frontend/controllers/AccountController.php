<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use common\models\ChangePassword;
use common\models\User;

class AccountController extends FrontendController {

    public function actionUpdate() {
        $model = User::find()->myAccount->one();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Your profile has been updated');
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionChangepassword() {
        $model = ChangePassword::find()->myAccount->one();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Your password has been updated');
            }
        }
        return $this->render('changepassword', ['model' => $model]);
    }

    public function actionPermission() {
        $userPermission = UserPermission::find()->where(['available' => 0])->all();
        return \yii\helpers\Json::encode($userPermission);
    }

    public function actionSupport() {
        return $this->render('support');
    }

    public function actionFaqs() {
        return $this->render('faqs');
    }
    
    public function actionLang($id){
        
    }

}
