<?php

namespace frontend\controllers;

use Yii;
use yii\web\HttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class FrontendController extends Controller {

    public function init() {
        if (isset($_REQUEST['lang'])){
            Yii::$app->session->set('lang', $_REQUEST['lang']);
            $this->redirect(['/site/index']);
        }
        if (Yii::$app->session->get('lang'))
            Yii::$app->language = Yii::$app->session->get('lang');
        parent::init();
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function setMessage($key, $type, $customText = null) {

        $kind = NULL;
        switch ($key) {
            case 'save' :
                $kind = 'save';
                break;
            case 'update' :
                $kind = 'update';
                break;
            case 'delete' :
                $kind = 'delete';
                break;
        }
        if (!is_null($kind))
            Yii::$app->session->setFlash($type, $customText !== null ? Yii::t('app', $customText) : Yii::$app->params['flashmsg'][$kind][$type]);
        else
            throw new BadRequestHttpException("Message Error !");
    }

}
