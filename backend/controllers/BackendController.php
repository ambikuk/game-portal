<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class BackendController extends Controller {

    protected $user;

    public function init() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl('/auth/login'));
        }
        $this->user = Yii::$app->user->identity;

        $this->checkActivation();

        return true;
    }

    protected function checkActivation() {
        return true;
    }

    public function setMessage($key, $type, $customText = null) {

        $kind = NULL;
        switch ($key) {
            case 'save' :
                $kind = 'save';
                break;
            case 'update' :
                $kind = 'update';
                break;
            case 'delete' :
                $kind = 'delete';
                break;
        }
        if (!is_null($kind))
            Yii::$app->session->setFlash($type, $customText !== null ? Yii::t('app', $customText) : Yii::$app->params['flashmsg'][$kind][$type]);
        else
            throw new BadRequestHttpException("Message Error !");
    }

}
