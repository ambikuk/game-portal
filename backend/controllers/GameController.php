<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use common\models\Game;
use common\models\GameComment;

class GameController extends BackendController {

    public function actionIndex() {
        $model = Game::find()->active()->orderBy('created_at desc');
        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->redirect(['index']);
        }
        return $this->render('form', [
                    'model' => $model,
        ]);
    }

    public function actionView($slug) {
        $model = Game::find()->bySlug($slug);
        $comment = new GameComment();
        if ($comment->load(Yii::$app->request->post())) {
            $comment->game_id = $model->id;
            if($comment->save()){
                $comment->comment = "";
                Yii::$app->getSession()->setFlash('success', 'Your comment has been added');
            }
        }
        return $this->render('view', [
                    'model' => $model,
                    'comment' => $comment
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->status = Game::STATUS_DELETED;
        if ($model->save())
            return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
