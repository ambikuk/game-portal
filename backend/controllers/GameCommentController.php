<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use common\models\GameComment;

class GameCommentController extends BackendController {

    public function actionIndex() {
        $model = GameComment::find();
        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->redirect(['index']);
        }
        return $this->render('form', [
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->status = GameComment::STATUS_DELETED;
        if ($model->save())
            return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = GameComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
