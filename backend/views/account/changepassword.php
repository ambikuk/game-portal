<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\Typeahead;
use kartik\widgets\DatePicker;

$this->title = 'Change Password';
?>
<h2><i class="fa fa-briefcase"></i> <?= $this->title ?></h2>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'old_password')->passwordInput() ?>
                <?= $form->field($model, 'new_password')->passwordInput() ?>
                <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="btn-toolbar">
                            <button class="btn-primary btn"><i class="fa fa-check"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>