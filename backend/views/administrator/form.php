<?php

use common\components\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? Yii::t('app', 'Create New Administrator') : Yii::t('app', 'Update Administrator'));
?>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'fullname') ?>
                <?= $form->field($model, 'email') ?>
                <?php if ($model->isNewRecord): ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                <?php endif; ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="btn-toolbar">
                            <button class="btn-primary btn"><i class="fa fa-check"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>
