<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Game Category');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-body">
                <?=
                GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model,
                            ]),
                    'columns' => [
                        'user.fullname',
                        'game.title',
                        'comment',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '<span class="pull-right">{update} {delete}</span>',
                        ]
                    ],
                    'tableOptions' => ['class' => 'table table-striped']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>