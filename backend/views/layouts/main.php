<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

$asset = AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?= Yii::$app->homeUrl ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>Game</b>Portal</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>Panel</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= $asset->baseUrl ?>/admin-lte/dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
                                    <span class="hidden-xs"><?php echo Yii::$app->loggedin->user->fullname; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= $asset->baseUrl ?>/admin-lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                                        <p>
                                            <?php echo Yii::$app->loggedin->user->fullname; ?>
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="<?= Yii::$app->urlManager->createUrl(['site/logout']) ?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= $asset->baseUrl ?>/admin-lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo Yii::$app->loggedin->user->fullname; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="<?= Yii::$app->urlManager->createUrl(['site/index']) ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i> <span>Games</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['game/index']) ?>"><i class="fa fa-circle-o"></i> Threads</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['game-category/index']) ?>"><i class="fa fa-circle-o"></i> Categories</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['game-comment/index']) ?>"><i class="fa fa-circle-o"></i> Comments</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= Yii::$app->urlManager->createUrl(['member/index']) ?>">
                                <i class="fa fa-users"></i> <span>Members</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::$app->urlManager->createUrl(['administrator/index']) ?>">
                                <i class="fa fa-user-secret"></i> <span>Administrators</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                <span>Account</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['account/update']) ?>"><i class="fa fa-circle-o"></i> Profile</a></li>
                                <li><a href="<?= Yii::$app->urlManager->createUrl(['account/changepassword']) ?>"><i class="fa fa-circle-o"></i> Change Password</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1><?= $this->title ?>
                        <small></small>
                    </h1>

                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </section>
                <!-- Main content -->
                <section class="content">
                    <?php echo $this->render('_notifications') ?><?= $content ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2014-2015 <a href="http://ambikuk.com">Ambikuk Com</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
