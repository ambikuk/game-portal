<?php

use common\models\User;
use common\models\Game;
use common\models\GameComment;
use common\models\GameCategory;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= User::find()->totalMember ?></h3>
                <p>Members</p>
            </div>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= Game::find()->totalGame ?></h3>
                <p>Posts</p>
            </div>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= GameComment::find()->totalGameComment ?></h3>
                <p>Comments</p>
            </div>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= GameCategory::find()->totalGameCategory ?></h3>
                <p>Categories</p>
            </div>
        </div>
    </div><!-- ./col -->
</div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Members</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th><?= Yii::t('app', 'Join Date') ?></th>
                                <th><?= Yii::t('app', 'Username') ?></th>
                                <th><?= Yii::t('app', 'Fullname') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (User::find()->latest10Members as $row): ?>
                                <tr>
                                    <td><?= Yii::$app->formatter->asDatetime($row->created_at) ?></td>
                                    <td><?= $row->username ?></td>
                                    <td><?= $row->fullname ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-6">
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Recently Added Posts</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php foreach (Game::find()->latest10 as $row): ?>
                        <li class="item">
                            <div class="product-info">
                                <a href="<?= $row->linkDetailFrontend ?>" class="product-title"><?= $row->title ?></a>
                                <span class="product-description">
                                    <?= $row->descShort ?>
                                </span>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>