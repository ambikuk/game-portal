<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Members');
?>
<div class="row">
    <div class="col-md-12"><p class="pull-left"><?= Html::a('<i class="fa fa-plus"></i> <span>' . Yii::t('app', 'New ' . $this->title) . '</span>', ['create'], ['class' => 'btn btn-default DTTT_button_text']) ?></p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-body">
                <?=
                GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model,
                            ]),
                    'columns' => [
                        'created_at:datetime',
                        'username',
                        'fullname',
                        'email',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '<span class="pull-right">{update} {delete}</span>',
                        ]
                    ],
                    'tableOptions' => ['class' => 'table table-striped']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>